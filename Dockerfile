# This file is a template, and might need editing before it works on your project.
# This Dockerfile installs a compiled binary into a bare system.
# You must either commit your compiled binary into source control (not recommended)
# or build the binary first as part of a CI/CD pipeline.

FROM ubuntu:focal

# add some stuff needed to compile DECaxp
RUN DEBIAN_FRONTEND=noninteractive \
    apt-get update && apt-get -y install build-essential cmake clang-9 && \
    apt-get -y install git
    
RUN ln -fs /usr/bin/clang++-9 /usr/bin/clang++ && ln -fs /usr/bin/clang-9 /usr/bin/clang

RUN git clone https://gitlab.com/JonathanBelanger/DECaxp.git

WORKDIR /usr/local/bin

# Change `app` to whatever your binary is called
# Add app .
#CMD ["./app"]
